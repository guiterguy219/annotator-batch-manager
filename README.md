# Annotator Batch Manager

This tool is meant to be used in conjunction with [this annotator tool](https://gitlab.com/byuhbll/apps/annotator). It takes advantage of the annotator's API to manage projects in large batches. It makes up for certain shortcomings in the user interface.

### To install Configuro:
```pip install -i https://pypi.lib.byu.edu configuro```