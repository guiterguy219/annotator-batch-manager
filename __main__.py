import requests
import argparse
import json
import time
import sys
import os
from getpass import getpass
from datetime import datetime as dt
from itertools import cycle
from collections import Counter
from tqdm import tqdm
from configuro import YamlConfig

parser = argparse.ArgumentParser(
    prog="Annotator Batch Manager",
    description="Manage batches of pipelines via API.",
)
parser.add_argument(
    "-a",
    "--autocreate",
    action="store_true",
    help="Automate creation of all available pipelines not yet created.",
)
parser.add_argument(
    "-d", "--delete-all", action="store_true", help="Delete all pipelines."
)
parser.add_argument(
    "-g",
    "--get_pipelines",
    action="store_true",
    help="Get all active pipelines formatted as JSON.",
)
parser.add_argument(
    "-s",
    "--single_user",
    action="store_true",
    help="Assigne each pipeline to single user instead of three.",
)
parser.add_argument(
    "-t",
    "--assignment-template",
    action="store",
    help="Path to assignment template file.",
)
parser.add_argument(
    "-m", "--modify", action="store", help="OPTIONS: [pipelines]"
)
args = parser.parse_args()

# GLOBAL VARIABLES
CONFIG = YamlConfig.build(
    yaml_files=["config/annotator_batch_mgr_config.yml", "config/auth.yml"]
)
MODE = "dev" if CONFIG.get("debug") else "production"
SINGLE_USER_TEMPLATE_PATH = CONFIG.get(
    f"{MODE}/templates/pipelines/create/single-assignment-pipeline"
)
MULTI_USER_TEMPLATE_PATH = CONFIG.get(
    f"{MODE}/templates/pipelines/create/multi-assignment-pipeline"
)


def login(host, user, password):
    """
    Starts new session with annotator.

    Args:
        user (str): Username for annotator account.
        password (str): Password for annotator account.

    Returns:
        response_dict (dict): Dictionary with authorization token.
    """
    url = f"{host}/api/user/login"

    payload = f'{{"password":"{password}","user_name":"{user}"}}'
    headers = {
        "Accept": "application/json",
        "Accept-Language": "en-US",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": "Bearer null",
        "Content-Type": "application/json",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
        "Content-Length": "43",
    }

    response = requests.request("POST", url, data=payload, headers=headers)

    if response.status_code == 200:
        response_dict = response.json()
        return response_dict
    else:
        print(response.text)
        raise requests.ConnectionError


def get_user(host, token):
    """
    Gets current session's user.

    Args:
        token (str): Authorization token returned at login.

    Returns:
        response_dict (dict): Dictionary of current user's info.
    """
    import requests

    url = f"{host}/api/user/self"

    headers = {
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Connection": "keep-alive",
    }

    response = requests.request("GET", url, headers=headers)

    if response.status_code == 200:
        response_dict = response.json()
        return response_dict
    else:
        print(response.text)
        raise requests.ConnectionError


def get_users(host, token):
    """
    Gets all users (not groups).

    Args:
        token (str): Authorization token returned at login.

    Returns:
        response_dict (dict): Dictionary of all user info.
    """
    url = f"{host}/api/user"

    headers = {
        "Host": "annotator.lib.byu.edu",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
    }

    response = requests.request("GET", url, headers=headers)

    if response.status_code == 200:
        response_dict = response.json()
        return response_dict
    else:
        print(response.text)
        raise requests.ConnectionError


def get_pipeline(host, token, id):
    url = f"{host}/api/pipeline/{id}"

    headers = {
        "Host": "annotator.lib.byu.edu",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
    }

    response = requests.request("GET", url, headers=headers)

    if response.status_code == 200:
        response_dict = response.json()
        return response_dict
    else:
        print(response.text)
        raise requests.ConnectionError


def get_pipelines(host, token):
    """
    Get all current pipelines.

    Args:
        token (str): Authorization token returned at login.

    Returns:
        response_dict (dict): Dictionary of all pipeline info.
    """
    url = f"{host}/api/pipeline"

    headers = {
        "Host": "annotator.lib.byu.edu",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
    }

    response = requests.request("GET", url, headers=headers)

    if response.status_code == 200:
        response_dict = response.json()
        return response_dict
    else:
        print(response.text)
        raise requests.ConnectionError


def logout(host, token):
    """
    End session with annotator.

    Args:
        token (str): Authorization token returned at login.

    Returns:
        response (Requests object): Response from API request.
    """
    url = f"{host}/api/user/logout"

    headers = {
        "Host": "annotator.lib.byu.edu",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Connection": "keep-alive",
        "Content-Length": "0",
    }

    response = requests.request("POST", url, headers=headers)

    if response.status_code == 200:
        return response
    else:
        print("\nFailed to logout!\n")
        print(response.text)
        raise requests.ConnectionError


def check_worker(host, token):
    url = f"{host}/api/worker"

    headers = {
        "Host": "annotator.lib.byu.edu",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
    }

    response = requests.request("GET", url, headers=headers)
    if response.status_code == 200:
        response = response.json()
    else:
        print(response.text)
        raise requests.ConnectionError
    now = dt.utcnow()
    try:
        worker_time = response["workers"][1]["timestamp"]
    except IndexError:
        worker_time = response["workers"][0]["timestamp"]
    last_life_sign = dt.strptime(worker_time, "%Y-%m-%dT%H:%M:%S.%f")
    time_elapsed = now - last_life_sign

    if time_elapsed.seconds < 10:
        return True
    else:
        return False


def load_pipeline_template(single: bool):
    if single:
        template_filename = SINGLE_USER_TEMPLATE_PATH
    else:
        template_filename = MULTI_USER_TEMPLATE_PATH

    template_path = os.path.join(
        os.path.dirname(__file__), template_filename
    )

    with open(template_path, "r") as f:
        pipeline_template = json.load(f)

        return pipeline_template


def new_pipeline(
    host,
    token,
    name,
    description,
    subset,
    assignees: list,
    labelTree,
    single: bool,
):
    """
    Create a single new pipeline via API for a mutliple assignees.

    Args:
        token (str): Authorization token returned at login.
        name (str): Name to be used for pipeline.
        description (str): Description for new pipeline.
        subset (str): Subset to be used as datasource.
        assignees (list): List of tuples with pair of ID
                          and formatted username of assignee.
        labelTree (int): ID of label tree.

    Returns:
        response (Requests object): Response from API request.
    """
    url = f"{host}/api/pipeline/start"

    pipeline_template = load_pipeline_template(single)
    pt = pipeline_template
    pt["name"] = name
    pt["description"] = description
    pt["elements"][0]["datasource"]["rawFilePath"] = subset
    for a in range(len(assignees)):
        pt["elements"][2 + a]["annoTask"][
            "name"
        ] = f"SIA Task ID: {subset} Iter {a + 1}"
        pt["elements"][2 + a]["annoTask"]["assignee"] = assignees[a][1]
        pt["elements"][2 + a]["annoTask"]["workerId"] = assignees[a][0]
        pt["elements"][2 + a]["annoTask"]["labelLeaves"][0][
            "id"
        ] = labelTree
        pt["elements"][2 + a]["annoTask"]["selectedLabelTree"] = labelTree
    # Export
    pt["elements"][2 + len(assignees)]["script"]["arguments"]["file_name"][
        "value"
    ] = f"anno_{subset}.csv"
    pt["elements"][2 + len(assignees)]["datasource"]["rawFilePath"][
        "file_name"
    ]["value"] = f"anno_{subset}.csv"

    payload = json.dumps(pt)

    headers = {
        "Host": "annotator.lib.byu.edu",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Content-Type": "application/json",
        "Content-Length": "1370",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
    }

    for i in range(10):
        if check_worker(host, token) or i == 9:
            response = requests.request(
                "POST", url, data=payload, headers=headers
            )
            break
        time.sleep(1)

    return response


def get_available_subsets(host, token, template_id):
    """
    Get all available subsets from API.

    Args:
        token (str): Authorization token returned at login.

    Returns:
        subsets (list): List of subsets.
    """
    url = f"{host}/api/pipeline/template/{template_id}"

    headers = {
        "Host": "annotator.lib.byu.edu",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
    }

    response = requests.request("GET", url, headers=headers)

    if response.status_code == 200:
        r_json = response.json()
    else:
        print(response.text)
        raise requests.ConnectionError

    if len(r_json) > 0:
        subsets = [
            s["name"]
            for s in r_json["elements"][0]["datasource"]["fileTree"][
                "children"
            ]
        ]
    else:
        subsets = []

    return subsets


def get_available_users(host, token, template_id):
    """
    Get all available users to be assigned from API.

    Args:
        token (str): Authorization token returned at login.
        template_id (int): ID corresponding to pipeline template being used.

    Returns:
        available_users (list): List of users.
    """
    url = f"{host}/api/pipeline/template/{template_id}"

    headers = {
        "Host": "annotator.lib.byu.edu",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
    }

    response = requests.request("GET", url, headers=headers)

    if response.status_code == 200:
        r_json = response.json()
    else:
        print(response.text)
        raise requests.ConnectionError

    available_users = r_json["availableGroups"]

    return available_users


def get_assignee(host, token, single: bool, custom_assign: list):
    """
    Generator: Get assignees formatted for use with new_pipeline.

    Args:
        token (str): Authorization token returned at login.
        single (bool): Assign single user instead of
                       replicating tasks among 3 users.
        custom_assign (list): Customize user assignment.

    Returns:
        assignee (tuple): ID and formatted username of assignee.
    """
    template_id = load_pipeline_template(single)["templateId"]

    users = {
        u["id"]: (u["id"], u["groupName"])
        for u in get_available_users(host, token, template_id)
        if "admin" not in u["groupName"] and "(group)" not in u["groupName"]
    }

    current_assignments = []
    user_dict = {v[1][:-7]: v[0] for k, v in users.items()}
    for p in export_pipelines(host, token, get_pipelines(host, token))[
        "pipelines"
    ]:
        for t in p["tasks"]:
            if "admin" in t["assignee"]:
                continue
            current_assignments.append(user_dict[t["assignee"]])
    current_assignments = Counter(current_assignments)

    def yield_users(user_list):
        for _, assignee in cycle(user_list.items()):
            yield assignee

    if not custom_assign:
        custom_assign = []
        custom_assign.append(input("Source of user assignment file: "))
        custom_assign.append(int(input("")))

        source = custom_assign[0]

        if not os.path.exists(source):
            assignment_template = {"pipeline_assignment_template": {}}
            pat = assignment_template["pipeline_assignment_template"]
            for i in range(int(args.custom_assign[1])):
                pat[f"pipeline_slot_{i + 1}"] = {}
                ps = pat[f"pipeline_slot_{i + 1}"]
                tasks = 1 if single else 3
                for j in range(tasks):
                    ps[f"assignment_slot_{j + 1}"] = None
            with open(source, "w") as f:
                json.dump(assignment_template, f, indent=4)
            print(
                "Current user assignment format has been written to file."
            )
            confirm = input(
                "Once finished modifying file, type Y to continue: "
            ).lower()
            if len(confirm) < 1:
                sys.exit()
            if confirm[0] != "y":
                sys.exit()
        with open(source, "r") as f:
            try:
                assignments = json.load(f)
            except json.decoder.JSONDecodeError:
                print("Invalid JSON file for custom user assignment.")
                sys.exit()
            i = -1
            specified_users = []
            gen_users = yield_users(users)
            for _, p in cycle(
                assignments["pipeline_assignment_template"].items()
            ):
                for _, i in p.items():
                    if i is not None:
                        specified_users.append(i)
                        yield users[i]
                    else:
                        a_cnt = [
                            v
                            for k, v in current_assignments.items()
                            if k not in specified_users
                        ]
                        avg_assignments = sum(a_cnt) / len(a_cnt)
                        next_user = next(gen_users)
                        while (
                            next_user[0] in specified_users
                            or current_assignments[next_user[0]]
                            > avg_assignments
                        ):
                            next_user = next(gen_users)
                        current_assignments[next_user[0]] += 1
                        yield next_user
    else:
        for _, assignee in cycle(users.items()):
            yield assignee


def create_all_available_pipelines(host, token, single: bool):
    """
    Create a pipeline for remaining available subsets
    not already in pipelines.

    Args:
        token (str): Authorization token returned at login.
        single (bool): Assign single user instead of
                       replicating tasks among 3 users.

    Returns:
        None
    """

    subsets_available = get_available_subsets(
        host, token, load_pipeline_template(single)["templateId"]
    )
    current_pipelines = get_pipelines(host, token)["pipes"]
    pipeline_subsets = [
        p["name"].split(": ")[-1] for p in current_pipelines
    ]
    new_subsets = list(set(subsets_available) - set(pipeline_subsets))

    # Initialize generator.
    assignee = get_assignee(host, token, single, args.custom_assign)

    for subset in tqdm(new_subsets):
        if single:
            assignees = [next(assignee)]
        else:
            assignees = [next(assignee) for _ in range(3)]
        name = f"SIA Project on Sbst.: {subset}"
        description = name
        labelTree = load_pipeline_template(single)["elements"][2][
            "annoTask"
        ]["selectedLabelTree"]

        response = new_pipeline(
            host,
            token,
            name,
            description,
            subset,
            assignees,
            labelTree,
            single,
        )
        if response.status_code == 200:
            time.sleep(3)
            pass
            continue
        else:
            print(response.text)
            resume = input("An error has occured. Continue?: ").lower()[0]
            if resume == "y":
                pass
                continue
            else:
                break


def modify_pipeline(host, token, payload, pipe_id):
    url = f"{host}/api/pipeline/update"

    headers = {
        "Accept": "application/json, text/plain, */*",
        "Authorization": f"Bearer {token}",
        "Content": "application/json",
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    return response


def modify_all_pipelines(host, token, pipeline_dict):
    path_to_config_dir = CONFIG.get(
        f"{MODE}/templates/pipelines/modify/directory"
    )
    if not os.path.isdir(path_to_config_dir):
        print("Invalid directory!")
        return

    templates_in_use = list(
        set([x["templateName"] for x in pipeline_dict["pipes"]])
    )

    for t in templates_in_use:
        pipes = [
            x for x in pipeline_dict["pipes"] if x["templateName"] == t
        ]
        config_path = os.path.join(path_to_config_dir, f"{t}.json")
        pipe_template = get_pipeline(host, token, pipes[0]["id"])
        if not os.path.exists(config_path):
            print(
                f"No config file found for {t}. Creating new config file..."
            )
            new_config = {
                "name": None,
                "description": None,
                "anno_tasks": {},
            }
            for i in range(len(pipe_template["elements"][2:-2])):
                at = {
                    "index": i + 2,
                    "name": None,
                    "instructions": None,
                    "userName": None,
                }
                new_config["anno_tasks"][f"anno_task{i + 1}"] = at
            with open(config_path, "w") as f:
                json.dump(new_config, f, indent=4)
            print("New config file created!")
        print(f"Please make changes in {t}.json.")
        c = input("Press any 'Y' to continue: ").lower()
        if len(c) < 1:
            return
        if c[0] != "y":
            return
        with open(config_path, "r") as f:
            config = json.load(f)
            for pipe in tqdm(pipes):
                p = get_pipeline(host, token, pipe["id"])
                for value in ["name", "description"]:
                    p[value] = config[value] if config[value] else p[value]
                for key, task in config["anno_tasks"].items():
                    at = p["elements"][task["index"]]["annoTask"]
                    for value in ["name", "instructions", "userName"]:
                        at[value] = (
                            task[value] if task[value] else at[value]
                        )
                payload = json.dumps(p)
                response = modify_pipeline(host, token, payload, pipe["id"])
                if response.status_code == 200:
                    pass
                    continue
                else:
                    print(response)
                    resume = input(
                        "An error has occured. Continue?: "
                    ).lower()[0]
                    if resume == "y":
                        pass
                        continue
                    else:
                        break


def delete_pipeline(host, token, pipeline_id):
    """
    Delete pipeline by pipeline ID.

    Args:
        token (str): Authorization token returned at login.
        pipeline_id (int): Pipeline identifier.

    Returns:
        response (Requests object): Response from API request.
    """
    url = f"{host}/api/pipeline/{pipeline_id}"

    headers = {
        "Host": "annotator.lib.byu.edu",
        "Accept": "application/json, text/plain, */*",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Authorization": f"Bearer {token}",
        "Connection": "keep-alive",
        "Cache-Control": "no-cache",
    }

    response = requests.request("DELETE", url, headers=headers)

    return response


def delete_all_pipelines(host, token):
    """
    Delete all pipelines.

    Args:
        token (str): Authorization token returned at login.

    Returns:
        None
    """
    print("Checking for pipelines in progress...")

    pipe_ids_no_start = []
    pipe_ids_started = []

    for p in tqdm(get_pipelines(host, token)["pipes"]):
        pipeline = get_pipeline(host, token, p["id"])
        try:
            started = False
            for e in pipeline["elements"][2:5]:
                progress = float(e["annoTask"]["progress"])
                started = started or progress > 0
            if started:
                pipe_ids_started.append(pipeline["id"])
            else:
                pipe_ids_no_start.append(pipeline["id"])
        except TypeError:
            pipe_ids_no_start.append(pipeline["id"])

    pipe_ids_to_delete = pipe_ids_no_start

    if len(pipe_ids_started) > 0:
        verify_phrase = "yes, delete"
        print(
            (
                f"WARNING! Annotations have been "
                f"started on {len(pipe_ids_started)} pipelines."
            )
        )
        verify = input(
            f"If you wish to delete these too, type '{verify_phrase}': "
        )
        if verify == verify_phrase:
            pipe_ids_to_delete = list(
                set(pipe_ids_to_delete + pipe_ids_started)
            )

    print(
        (
            f"Deleting {len(pipe_ids_to_delete)} of "
            f"{len(pipe_ids_no_start) + len(pipe_ids_started)} pipelines..."
        )
    )

    for id in tqdm(pipe_ids_to_delete):
        response = delete_pipeline(host, token, id)
        if response.status_code == 200:
            pass
            continue
        else:
            print(response)
            resume = input("An error has occured. Continue?: ").lower()[0]
            if resume == "y":
                pass
                continue
            else:
                break


def export_pipelines(host, token, pipeline_dict):
    """
    Export a summary of all active pipelines.

    Args:
        token (str): Authorization token returned at login.
        pipeline_dict (dict): Current pipelines
            from function get_pipelines

    Returns:
        pipelines_out (dict): Completed summary of pipelines.
    """
    pipelines_out = {}
    pipelines_out["total_pipeline_count"] = len(pipeline_dict["pipes"])
    pipelines_out["total_task_count"] = 0
    pipelines_out["total_progress"] = 0.0
    pipelines_out["assignees"] = []
    pipelines_out["pipelines"] = []
    for p in pipeline_dict["pipes"]:
        p_in = get_pipeline(host, token, p["id"])
        p_out = {}
        p_out["id"] = p_in["id"]
        p_out["name"] = p_in["name"]
        p_out["description"] = p_in["description"]
        p_out["date"] = p_in["timestamp"]
        p_out["logfile_path"] = p_in["logfilePath"]
        p_out["path_to_subset"] = p_in["elements"][0]["datasource"][
            "rawFilePath"
        ]
        p_out["subset_name"] = p_out["path_to_subset"].split("/")[-1]

        tasks = []
        for e in p_in["elements"][2:-2]:
            pipelines_out["total_task_count"] += 1
            t = {}
            t["id"] = e["annoTask"]["id"]
            t["name"] = e["annoTask"]["name"]
            t["assignee"] = e["annoTask"]["userName"]
            t["progress"] = e["annoTask"]["progress"]
            pipelines_out["assignees"].append(
                (t["assignee"], t["progress"])
            )
            prog = t["progress"] if t["progress"] else 0
            pipelines_out["total_progress"] += prog
            tasks.append(t)

        p_out["tasks"] = tasks
        p_out["label_tree"] = p_in["elements"][2]["annoTask"][
            "labelLeaves"
        ][0]["name"]
        p_out["output_filename"] = p_in["elements"][-2]["script"][
            "arguments"
        ]["file_name"]["value"]
        pipelines_out["pipelines"].append(p_out)

    try:
        pipelines_out["total_progress"] /= pipelines_out["total_task_count"]
    except ZeroDivisionError:
        pipelines_out["total_progress"] = 0
    pipelines_out["total_progress"] = "{0:.2f}%".format(
        pipelines_out["total_progress"]
    )
    summed_assignees = Counter([i[0] for i in pipelines_out["assignees"]])
    processed_assignees = []
    for a1, count in summed_assignees.items():
        individual_progress = 0
        for a2 in pipelines_out["assignees"]:
            if a1 == a2[0]:
                prog = a2[1] if a2[1] else 0
                individual_progress += prog
        individual_progress /= count
        pa = {
            "assignee": a1,
            "task_count": count,
            "total_progress": round(individual_progress, 2),
        }
        processed_assignees.append(pa)
    processed_assignees = sorted(
        processed_assignees, key=lambda x: x["total_progress"], reverse=True
    )
    pipelines_out["assignees"] = processed_assignees

    return pipelines_out


if __name__ == "__main__":
    HOST = CONFIG.get(f"{MODE}/host")
    TOKEN = False
    try:
        username = (
            CONFIG.get(f"credentials/{MODE}/user")
            if len(CONFIG.get(f"credentials/{MODE}/user")) > 0
            else input("Username: ")
        )
        passwd = (
            CONFIG.get(f"credentials/{MODE}/password")
            if len(CONFIG.get(f"credentials/{MODE}/password")) > 0
            else getpass()
        )
        TOKEN = login(HOST, username, passwd)["token"]
    except requests.ConnectionError:
        parser.print_help()
        sys.exit()
    if args.get_pipelines:
        out_file = dt.strftime(
            dt.now(), f"{MODE}-status_%d-%m-%Y_%H-%M-%S.json"
        )
        out_dir = CONFIG.get("output_dir_path")
        out_path = os.path.join(out_dir, out_file)
        with open(out_path, "w") as f:
            json.dump(
                export_pipelines(HOST, TOKEN, get_pipelines(HOST, TOKEN)),
                f,
                indent=4,
            )
    elif args.modify:
        if args.modify.lower() == "pipelines":
            modify_all_pipelines(HOST, TOKEN, get_pipelines(HOST, TOKEN))
    elif args.autocreate:
        print("Creating new pipelines from available datasets...")
        single = CONFIG.get(f"{MODE}/single-user")
        create_all_available_pipelines(HOST, TOKEN, single)
    elif args.delete_all:
        verify_phrase = "confirm deletion"
        print("Are you sure you want to delete all pipelines?")
        try:
            verify = input(f"To confirm, please type '{verify_phrase}': ")
            if verify == verify_phrase:
                delete_all_pipelines(HOST, TOKEN)
                print("This operation may have delayed results...")
            else:
                print("Cancelling request...")
        except KeyboardInterrupt:
            print("\nCancelling request...")
    else:
        parser.print_help()
    logout(HOST, TOKEN)
